class Arbol {

    constructor() {
        this.nodoPadre = this.agregarNodoPadre();
        this.nivel = prompt("Ingresa el nivel que deseas buscar ");
        this.busquedaElemento;
        this.buscarNodos = [];
        this.caminoNodo = '';
        this.sumarCamino = 0;
    }

    agregarNodoPadre() {
        var nodo = new Nodo(null, null, 65);
        return nodo;
    }

    agregarNodo(nodoPadre, posicion, valor) {
        var nodo = new Nodo(nodoPadre, posicion, valor);
        return nodo;
    }

    verificarNivelHijos(nodo) { 

        if (nodo.nivel == this.nivel)
            this.buscarNodos.push(nodo.valor);

        if (nodo.hasOwnProperty('hI'))
            this.verificarNivelHijos(nodo.hI);

        if (nodo.hasOwnProperty('hD'))
            this.verificarNivelHijos(nodo.hD);

        return this.buscarNodos;

    }

    Buscarvalor(buscarElemento, nodo) { 

        if (nodo.valor == buscarElemento)
            this.busquedaElemento = nodo;

        if (nodo.hasOwnProperty('hI'))
            this.buscarValor(buscarElemento, nodo.hI);

        if (nodo.hasOwnProperty('hD'))
            this.buscarValor(buscarElemento, nodo.hD);

        return this.busquedaElemento;
    }

    BuscarcaminoNodo(nodo) {
        if (nodo.padre != null) {
            this.caminoNodo = this.caminoNodo + ',' + nodo.padre.valor;
            this.buscarCaminoNodo(nodo.padre)
        }
        return this.busquedaElemento.valor + ' ' + this.caminoNodo;
    }

    SumarcaminoNodo(nodo) { 
        if (nodo.padre != null) {
            this.sumarCamino = this.sumarCamino + nodo.padre.valor;
            this.sumarCaminoNodo(nodo.padre)
        }
        return this.busquedaElemento.valor + this.sumarCamino;
    }
}