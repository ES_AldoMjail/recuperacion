class Nodo{
    constructor(nodoPadre = null, posicion=null, valor) {
        this.padre = nodoPadre;
        this.posicion = posicion;
        this.valor = valor;
        if(nodoPadre == null)
            this.nivel = 0;
        else
            this.nivel = nodoPadre.nivel + 1;
    }
}